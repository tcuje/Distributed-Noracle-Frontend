export enum GraphInteractionMode {
  SelectAndNavigate,
  DragAndZoom,
  AddQuestion,
  AddRelation,
  EditAndAssess
}
