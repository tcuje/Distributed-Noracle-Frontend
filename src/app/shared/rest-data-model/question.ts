/**
 * Created by bgoeschlberger on 11.09.2017.
 */
export class Question {
  questionId: string;
  text: string;
  spaceId: string;
  authorId: string;
  timestampCreated: string;
  timestampLastModified: string;

  followUps: number;

  constructor() {
  }
}
