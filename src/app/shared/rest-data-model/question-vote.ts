/**
 * Created by bgoeschlberger on 11.09.2017.
 */
export class QuestionVote {
  value: number;
  voterAgentId: string;
}
