/**
 * Created by bgoeschlberger on 11.09.2017.
 */
export class RelationVote {
  value: number;
  voterAgentId: string;
}
