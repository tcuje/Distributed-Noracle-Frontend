export const environment = {
  production: true,
  redirectUrl: 'http://dbis.rwth-aachen.de/noracle/login',
  hostUrl: ['http://steen.informatik.rwth-aachen.de:9089']
};
