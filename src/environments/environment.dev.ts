export const environment = {
  production: true,
  redirectUrl: 'http://steen.informatik.rwth-aachen.de:9082/fileservice/v2.2.5/files/noracle/login',
  hostUrl: ['http://steen.informatik.rwth-aachen.de:9082']
};
