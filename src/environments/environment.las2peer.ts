export const environment = {
  production: true,
  redirectUrl: 'http://localhost:9082/fileservice/v2.2.5/files/noracle/login',
  hostUrls: ['http://localhost:9082']
};
